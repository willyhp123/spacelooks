<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_p');
            $table->string('nama_pendidikan_p')->nullable();
            $table->string('nama_universitas_p')->nullable();
            $table->string('tanggal_mulai_p')->nullable();
            $table->string('tanggal_akhir_p')->nullable();
            $table->string('domisili_p')->nullable();
            $table->string('foto_p')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendidikans');
    }
};
