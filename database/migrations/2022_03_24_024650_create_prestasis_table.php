<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasis', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_prestasi');
            $table->string('nama_prestasi');
            $table->string('nama_perusahaan_prestasi');
            $table->string('tanggal_awal_prestasi');
            $table->string('tanggal_akhir_prestasi');
            $table->string('domisili_prestasi');
            $table->string('foto_prestasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestasis');
    }
};
