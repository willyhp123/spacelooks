<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengalaman_kegiatans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_p_kegiatan');
            $table->string('nama_kegiatan');
            $table->string('nama_perusahaan_p_kegiatan');
            $table->string('tanggal_awal_p_kegiatan');
            $table->string('tanggal_akhir_p_kegiatan');
            $table->string('domisili_p_kegiatan');
            $table->string('foto_p_kegiatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengalaman_kegiatans');
    }
};
