<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sertifikats', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_sertifikat');
            $table->string('nama_sertifikat');
            $table->string('nama_perusahaan_sertifikat');
            $table->string('tanggal_mulai_sertifikat');
            $table->string('tanggal_akhir_sertifikat');
            $table->string('domisili_sertifikat');
            $table->string('foto_sertifikat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sertifikats');
    }
};
