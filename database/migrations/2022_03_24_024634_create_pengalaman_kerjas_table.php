<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengalaman_kerjas', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_p_kerja');
            $table->string('nama_pengalaman_kerja')->nullable();
            $table->string('nama_perusahaan_p_kerja')->nullable();
            $table->string('tanggal_mulai_p_kerja')->nullable();
            $table->string('tanggal_akhir_p_kerja')->nullable();
            $table->string('domisili_p_kerja')->nullable();
            $table->string('foto_p_kerja')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengalaman_kerjas');
    }
};
