<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_penggunas', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email');
            $table->string('no_hp');
            $table->string('password');
            $table->string('tentang');
            $table->integer('kunjungan_biodata');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_penggunas');
    }
};
