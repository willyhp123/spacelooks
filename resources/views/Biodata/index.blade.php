@extends('Biodata.layouts.header')
@section('title','biodata User')
@section('content')
  <main class="main-content position-relative border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur" data-scroll="false">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-white active" aria-current="page">Biodata</li>
          </ol>
          <h6 class="font-weight-bolder text-white mb-0"></h6>
        </nav>
        
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <img src="{{ asset('foto_user/'.$datas['foto_user']) }}" alt="" width="150px" height="150px" style="border-radius:100px;border:1px solid;margin-bottom:10px;">
              <h6>{{ $datas['username'] }}</h6>
              <p style="margin-top: -10px;">{{ $datas['email'] }} | {{ $datas['no_hp'] }}</p>
            
            </div>
            <div class="border"></div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="p-4">
                  <h5>Tentang</h2>
                  <p>{{ $datas['tentang'] }}
                  </p>
              </div>
              <div class="border"></div>
              <div class="p-4">
               
                <h5>Pendidikan</h5>
                @foreach ($pendidikan as $val_p)
                <img src="{{ asset('foto_pendidikan/'.$val_p->foto_p) }}"  width="100px" height="100px">
                <h6>{{ $val_p->nama_pendidikan_p }}</h5>
                <p style="margin-top:-10px"><small>{{ $val_p->nama_universitas_p }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p->tanggal_awal_p }}-{{ $val_p->tanggal_akhir_p }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p->domisili_p }}</small></p>
                @endforeach
                 
                
              </div>
            </div>
            <div class="border"></div>
              <div class="p-4">
                <h5>Pengalaman Kerja</h5>
                @foreach ($pengalaman_kerja as $val_p_kerja )
                <img src="{{ asset('foto_pengalaman_kerja/'.$val_p_kerja->foto_p_kerja) }}"  width="100px" height="100px">
                <h6>{{ $val_p_kerja->nama_pengalaman_kerja }}</h5>
                <p style="margin-top:-10px"><small>{{ $val_p_kerja->nama_perusahaan_p_kerja }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p_kerja->tanggal_mulai_p_kerja }}-{{ $val_p_kerja->tanggal_akhir_p_kerja }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p_kerja->domisili_p_kerja }}</small></p>
                @endforeach
              </div>
              <div class="border"></div>
              <div class="p-4">
                <h5>Pengalaman Kegiatan</h5>
                @foreach ($pengalaman_kegiatan as $val_p_kegiatan)
                <img src="{{ asset('foto_kegiatan/'.$val_p_kegiatan->foto_p_kegiatan) }}" alt="" width="100px" height="100px">
                <h6>{{ $val_p_kegiatan->nama_kegiatan_p_kegiatan }}</h6>
                <p style="margin-top:-10px"><small>{{ $val_p_kegiatan->nama_perusahaan_p_kegiatan }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p_kegiatan->tanggal_awal_p_kegiatan }} | {{ $val_p_kegiatan->tanggal_akhir_p_kegiatan }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_p_kegiatan->domisili_p_kegiatan }}</small></p>
                @endforeach
               
               
              </div>
              <div class="border"></div>
              <div class="p-4">
                <h5>Sertifikat</h5>
                @foreach ($sertifikat as $val_sertifikat)
                <img src="{{ asset('foto_sertifikat/'.$val_sertifikat->foto_sertifikat) }}" alt="" width="150px" height="150px">
                <h6>{{ $val_sertifikat->nama_sertifikat }}</h6>
                <p style="margin-top:-10px"><small>{{ $val_sertifikat->nama_perusahaan_sertifikat }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_sertifikat->tanggal_awal_sertifikat }} | {{ $val_sertifikat->tanggal_akhir_sertifikat }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_sertifikat->domisili_sertifikat }}</small></p>
                @endforeach
              </div>
              <div class="border"></div>
              <div class="p-4">
                <h5>Prestasi</h5>
                @foreach ($prestasi as $val_prestasi)
                <img src="{{ asset('foto_prestasi/'.$val_prestasi->foto_prestasi) }}" alt="" width="150px" height="150px">
                <h6>{{ $val_prestasi->nama_prestasi }}</h6>
                <p style="margin-top:-10px"><small>{{ $val_prestasi->nama_perusahaan_prestasi }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_prestasi->tanggal_awal_prestasi }} - {{ $val_prestasi->tanggal_akhir_prestasi }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $val_prestasi->domisili_prestasi }}</small></p>
                @endforeach
              </div>
          </div>
        </div>
      </div>
    
@endsection  
 
