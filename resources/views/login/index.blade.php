@extends('login.layouts.header')

@section('title','Login form')
@section('content')
	<body>
	<section class="mt-5">
		<div class="container">
			
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-10">
					<div class="wrap d-md-flex">
						<div class="img" style="background-image: url({{ asset('login/images/bg-1.jpg') }});">
			      </div>
						<div class="login-wrap p-4 p-md-5">
			      	<div class="d-flex">
			      		<div class="w-100">
			      			<center><h3 class="mb-4">LOGIN FORM</h3></center>
							  @if (session()->get('success'))
								  <div class="alert alert-success">
									  <p>{{ session()->get('success') }}</p>
								  </div>
							  @endif
							  @if (session()->get('fail'))
							  <div class="alert alert-danger">
								  <p>{{ session()->get('fail') }}</p>
							  </div>
						  @endif
			      		</div>	
			      	</div>
							<form method="post" class="signin-form" action="{{ route('check') }}">
								@csrf
			      		<div class="form-group mb-3">
			      			<label class="label" for="name">Username</label>
			      			<input type="text" name ="username" class="form-control" placeholder="Username" required>
			      		</div>
		            <div class="form-group mb-3">
		            	<label class="label" for="password">Password</label>
		              <input type="password" name ="password" class="form-control" placeholder="Password" required>
		            </div>
		            <div class="form-group">
		            	<button type="submit" class="form-control btn btn-primary rounded submit px-3">Sign In</button>
		            </div>
							</form>
		            <div class="form-group d-md-flex">
		            	<div class="w-50 text-left">
			            	<label class="checkbox-wrap checkbox-primary mb-0">Remember Me
									  <input type="checkbox" checked>
									  <span class="checkmark"></span>
										</label>
									</div>
									<div class="w-50 text-md-right">
										<a href="#">Forgot Password</a>
									</div>
		            </div>
		          </form>
		          <p class="text-center">Not a member? <a href="{{ route('register') }}">Sign Up</a></p>
		        </div>
		      </div>
				</div>
			</div>
		</div>
	</section>
@endsection

