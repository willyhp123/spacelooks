@extends('login.layouts.header')

@section('title','Register form')
@section('content')
	<body>
	<section class="mt-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-10">
					<div class="wrap d-md-flex">
						<div class="img" style="background-image: url({{ asset('login/images/bg-1.jpg') }});">
			      </div>
						<div class="login-wrap p-4 p-md-5">
			      	<div class="d-flex">
			      		<div class="w-100">
			      			<center><h3 class="mb-4">REGISTER FORM</h3></center>
							  @if (session()->get('success'))
								  <div class="alert alert-success">
									  <p>{{ session()->get('success') }}</p>
								  </div>
							  @endif
							  @if (session()->get('fail'))
							  <div class="alert alert-danger">
								  <p>{{ session()->get('fail') }}</p>
							  </div>
						  @endif
			      		</div>
								
			      	</div>
					<form method="post" class="signin-form" action="{{ route('register_user') }}"
					 enctype="multipart/form-data">
								@csrf
								<div class="form-group mb-3">
									<label class="label" for="name">Foto</label><br>
									<input type="file" name="foto_user" >
								</div>
			      		<div class="form-group mb-3">
			      			<label class="label" for="name">Username</label>
			      			<input type="text" name ="username" class="form-control" 
							  placeholder="Username" required>
			      		</div>
                          <div class="form-group mb-3">
                            <label class="label" for="name">email</label>
                            <input type="email" name ="email" class="form-control" 
							placeholder="Email" required>
                        </div>
                        <div class="form-group mb-3">
                            <label class="label" for="name">No Handphone</label>
                            <input type="no_hp" name ="no_hp" class="form-control" 
							placeholder="Email" required>
                        </div>
		            <div class="form-group mb-3">
		            	<label class="label" for="password">Password</label>
		              <input type="password" name ="password" class="form-control"
					   placeholder="Password" required>
		            </div>
                    <div class="form-group mb-3">
		            	<label class="label" for="password">Confirm Password</label>
		              <input type="password" name ="confirm_password" class="form-control"
					   placeholder="Password" required>
		            </div>
		            <div class="form-group">
		            	<button type="submit" class="form-control btn btn-primary rounded submit px-3">Sign In</button>
		            </div>
		          </form>
		          <p class="text-center">Have Member? <a href="{{ route('login') }}">login</a></p>
		        </div>
		      </div>
				</div>
			</div>
		</div>
	</section>
@endsection

