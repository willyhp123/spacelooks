<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_tentang-{{ $datas['id'] }}'><i class="fa fa-edit"></i></a>
<div class="modal fade" id="m_tentang-{{ $datas['id'] }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Tentang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{ route('update_tentang',$datas['id']) }}" method="post">
                @csrf
            <div class="modal-body">
                <div class="form-control mb-2">
                    <label for="">Tentang</label>
                    <textarea name="tentang" cols="30" class ="form-control" rows="10">{{ $datas['tentang'] }}</textarea>
                </div>
            </div>
            <div class="footer p-2">
              <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-danger btn-xs">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>