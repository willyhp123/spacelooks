<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_update_profile-{{ $datas['id'] }}'><i class="fa fa-edit"></i></a>
<div class="modal fade" id="m_update_profile-{{ $datas['id'] }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{ route('edit_profile',$datas['id']) }}" method="post" enctype="multipart/form-data">
            @csrf
              <div class="modal-body">
              <div class="form-control mb-2">
                <label for="">Foto</label><br>
                <img src="{{ asset('foto_user/'.$datas['foto_user']) }}" width="100px" height="100px" alt="">
                <div style="height: 10px"></div>
                <input type="file" name="foto_user" class ="form-control form-control-sm">
              </div>
              <div class ="form-control mb-2">
                <label for="">Username</label>
                <input type="text" name="username" value ="{{ $datas['username'] }}"
                 class="form-control form-control-sm" >
              </div>
              <div class="form-control mb-2">
                <label for="">No Hp</label>
                <input type="text" name="no_hp" value ="{{ $datas['no_hp'] }}" 
                class ="form-control form-control-sm">
              </div>
            
            </div>
            <div class="footer p-2">
              <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-danger btn-xs">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>