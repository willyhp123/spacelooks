<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_pendidikan-{{ $pendidikans->id }}'><i class="fa fa-edit"></i></a>
<div class="modal fade" id="m_pendidikan-{{ $pendidikans->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Pendidikan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{ route('update_pendidikan',$pendidikans->id) }}" method="post" enctype="multipart/form-data">
            @csrf
              <div class="modal-body">
              <div class="form-control mb-2">
                <label for="">Foto</label><br>
                <img src="{{ asset('foto_pendidikan/'.$pendidikans->foto_p) }}" width="100px" height="100px" alt="">
                <div style="height: 10px"></div>
                <input type="file" name="foto_p" class ="form-control form-control-sm">
              </div>
              <div class ="form-control mb-2">
                <label for="">Nama Pendidikan</label>
                <input type="text" name="nama_pendidikan_p" value ="{{ $pendidikans->nama_pendidikan_p }}"
                 class="form-control form-control-sm" >
              </div>
              <div class="form-control mb-2">
                <label for="">Nama Universitas</label>
                <input type="text" name="nama_universitas_p" value ="{{ $pendidikans->nama_universitas_p }}" 
                class ="form-control form-control-sm">
              </div>
              <div class="form-control mb-2">
                <label for="">Tanggal awal</label>
                <input type="date" name="tanggal_awal_p" class ="form-control form-control-sm"
                 value ="{{ $pendidikans->tanggal_awal_p }}">
              </div>
              <div class="form-control mb-2 ">
                <label for="">Tanggal akhir</label>
                <input type="date" name="tanggal_akhir_p" class ="form-control form-control-sm" 
                value ="{{ $pendidikans->tanggal_akhir_p }}">
              </div>
              <div class="form-control mb-2">
                <label for="">Domisili</label>
                <input type="text" name="domisili_p" class ="form-control form-control-sm"
                value ="{{ $pendidikans->domisili_p }}"
                >
              </div>
            </div>
            <div class="footer p-2">
              <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-danger btn-xs">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>