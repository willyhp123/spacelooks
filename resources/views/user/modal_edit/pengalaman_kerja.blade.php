<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_pengalaman_kerja-{{ $pengalaman_kerjas->id }}'><i class="fa fa-edit"></i></a>
                      <div class="modal fade" id="m_pengalaman_kerja-{{ $pengalaman_kerjas->id }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title">Edit Pengalaman_kerja</h4>
                                      <button type="button" class="close" data-dismiss="modal" 
                                      aria-hidden="true">&times;</button>
                                  </div>
                                  <form action="{{ route('update_pengalaman_kerja',$pengalaman_kerjas->id) }}" method="post" enctype="multipart/form-data">
                                  @csrf
                                    <div class="modal-body">
                                    <div class="form-control mb-2">
                                      <label for="">Foto</label><br>
                                      <img src="{{ asset('foto_pengalaman_kerja/'.$pengalaman_kerjas->foto_p_kerja) }}" width="100px" height="100px" alt="">
                                      <div style="height: 10px"></div>
                                      <input type="file" name="foto_p_kerja" class ="form-control form-control-sm">
                                    </div>
                                    <div class ="form-control mb-2">
                                      <label for="">Nama Pengalaman Kerja</label>
                                      <input type="text" name="nama_pengalaman_kerja" value ="{{ $pengalaman_kerjas->nama_pengalaman_kerja }}"
                                       class="form-control form-control-sm" >
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Nama Perusahaan</label>
                                      <input type="text" name="nama_perusahaan_p_kerja" value ="{{ $pengalaman_kerjas->nama_perusahaan_p_kerja }}" 
                                      class ="form-control form-control-sm">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal mulai</label>
                                      <input type="date" name="tanggal_mulai_p_kerja" class ="form-control form-control-sm"
                                       value ="{{ $pengalaman_kerjas->tanggal_mulai_p_kerja }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal akhir</label>
                                      <input type="date" name="tanggal_akhir_p" class ="form-control form-control-sm" 
                                      value ="{{ $pengalaman_kerjas->tanggal_akhir_p_kerja }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Domisili</label>
                                      <input type="text" name="domisili_p_kerja" class ="form-control form-control-sm"
                                      value ="{{ $pengalaman_kerjas->domisili_p_kerja }}"
                                      >
                                    </div>
                                  </div>
                                  <div class="footer p-2">
                                    <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-danger btn-xs">Update</button>
                                  </div>
                                  </form>
                              </div>
                          </div>
                      </div>