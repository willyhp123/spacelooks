<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_pengalaman_kegiatan-{{ $pengalaman_kegiatans->id }}'><i class="fa fa-edit"></i></a>
                      <div class="modal fade" id="m_pengalaman_kegiatan-{{ $pengalaman_kegiatans->id }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title">Edit Pengalaman Kegiatan</h4>
                                      <button type="button" class="close" data-dismiss="modal" 
                                      aria-hidden="true">&times;</button>
                                  </div>
                                  <form action="{{ route('update_kegiatan',$pengalaman_kegiatans->id) }}" method="post" enctype="multipart/form-data">
                                  @csrf
                                    <div class="modal-body">
                                    <div class="form-control mb-2">
                                      <label for="">Foto</label><br>
                                      <img src="{{ asset('foto_pengalaman_kegiatan/'.$pengalaman_kegiatans->foto_p_kegiatan) }}" width="100px" height="100px" alt="">
                                      <div style="height: 10px"></div>
                                      <input type="file" name="foto_p_kegiatan" class ="form-control form-control-sm">
                                    </div>
                                    <div class ="form-control mb-2">
                                      <label for="">Nama Kegiatan</label>
                                      <input type="text" name="nama_kegiatan_p_kegiatan" value ="{{ $pengalaman_kegiatans->nama_kegiatan_p_kegiatan }}"
                                       class="form-control form-control-sm" >
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Nama Perusahaan</label>
                                      <input type="text" name="nama_perusahaan_p_kegiatan" value ="{{ $pengalaman_kegiatans->nama_perusahaan_p_kegiatan }}" 
                                      class ="form-control form-control-sm">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal mulai</label>
                                      <input type="date" name="tanggal_awal_p_kegiatan" class ="form-control form-control-sm"
                                       value ="{{ $pengalaman_kegiatans->tanggal_awal_p_kegiatan }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal akhir</label>
                                      <input type="date" name="tanggal_akhir_p_kegiatan" class ="form-control form-control-sm" 
                                      value ="{{ $pengalaman_kegiatans->tanggal_akhir_p_kegiatan }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Domisili</label>
                                      <input type="text" name="domisili_p_kegiatan" class ="form-control form-control-sm"
                                      value ="{{ $pengalaman_kegiatans->domisili_p_kegiatan }}"
                                      >
                                    </div>
                                  </div>
                                  <div class="footer p-2">
                                    <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-danger btn-xs">Update</button>
                                  </div>
                                  </form>
                              </div>
                          </div>
                      </div>