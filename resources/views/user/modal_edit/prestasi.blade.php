    <a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_prestasi-{{ $prestasis->id }}'><i class="fa fa-edit"></i></a>
                      <div class="modal fade" id="m_prestasi-{{ $prestasis->id }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title">Edit Prestasi</h4>
                                      <button type="button" class="close" data-dismiss="modal" 
                                      aria-hidden="true">&times;</button>
                                  </div>
                                  <form action="{{ route('update_prestasi',$prestasis->id) }}" method="post"
                                    enctype="multipart/form-data"
                                    >
                                    @csrf
                                  <div class="modal-body">
                                    <div class="form-control mb-2">
                                      <label for="">Foto</label><br>
                                      <img src="{{ asset('foto_prestasi/'.$prestasis->foto_prestasi) }}" 
                                      width="100px" height="100px" alt="">
                                      <div style="height: 10px"></div>
                                      <input type="file" name="foto_prestasi" class ="form-control form-control-sm">
                                    </div>
                                    <div class ="form-control mb-2">
                                      <label for="">Nama Prestasi</label>
                                      <input type="text" name="nama_prestasi" value ="{{ $prestasis->nama_prestasi }}"
                                       class="form-control form-control-sm" >
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Nama Perusahaan</label>
                                      <input type="text" name="nama_perusahaan_prestasi" value ="{{ $prestasis->nama_perusahaan_prestasi }}" 
                                      class ="form-control form-control-sm">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal mulai</label>
                                      <input type="date" name="tanggal_awal_prestasi" class ="form-control form-control-sm"
                                       value ="{{ $prestasis->tanggal_awal_prestasi }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal akhir</label>
                                      <input type="date" name="tanggal_akhir_prestasi" class ="form-control form-control-sm" 
                                      value ="{{ $prestasis->tanggal_akhir_prestasi }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Domisili</label>
                                      <input type="text" name="domisili_prestasi" class ="form-control form-control-sm"
                                      value ="{{ $prestasis->domisili_prestasi }}"
                                      >
                                    </div>
                                  </div>
                                  <div class="footer p-2">
                                    <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-danger btn-xs">Update</button>
                                  </div>
                                  </form>
                              </div>
                          </div>
                      </div>