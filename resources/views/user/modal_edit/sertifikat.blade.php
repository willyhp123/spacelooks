<a class="btn btn-warning btn-xs" data-toggle="modal" href='#m_sertifikat-{{ $sertifikats->id }}'><i class="fa fa-edit"></i></a>

                      <div class="modal fade" id="m_sertifikat-{{ $sertifikats->id }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title">Edit sertifikat</h4>
                                      <button type="button" class="close" data-dismiss="modal" 
                                      aria-hidden="true">&times;</button>
                                  </div>
                                  <form action="{{ route('update_sertifikat',$sertifikats->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                    <div class="modal-body">
                                    <div class="form-control mb-2">
                                      <label for="">Foto</label>
                                      <img src="{{ asset('foto_sertifikat/'.$sertifikats->foto_sertifikat) }}" 
                                      width="100px" height="100px" alt="">
                                      <div style="height: 10px"></div>
                                      <input type="file" name="foto_sertifikat" class ="form-control form-control-sm">
                                    </div>
                                    <div class ="form-control mb-2">
                                      <label for="">Nama sertifikat</label>
                                      <input type="text" name="nama_sertifikat" value ="{{ $sertifikats->nama_sertifikat }}"
                                       class="form-control form-control-sm" >
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Nama Perusahaan</label>
                                      <input type="text" name="nama_perusahaan_sertifikat" value ="{{ $sertifikats->nama_perusahaan_sertifikat }}" 
                                      class ="form-control form-control-sm">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal mulai</label>
                                      <input type="date" name="tanggal_awal_sertifikat" class ="form-control form-control-sm"
                                       value ="{{ $sertifikats->tanggal_awal_sertifikat }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Tanggal akhir</label>
                                      <input type="date" name="tanggal_akhir_sertifikat" class ="form-control form-control-sm" 
                                      value ="{{ $sertifikats->tanggal_akhir_sertifikat }}">
                                    </div>
                                    <div class="form-control mb-2">
                                      <label for="">Domisili</label>
                                      <input type="text" name="domisili_sertifikat" class ="form-control form-control-sm"
                                      value ="{{ $sertifikats->domisili_sertifikat }}"
                                      >
                                    </div>
                                  </div>
                                  <div class="footer p-2">
                                    <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-danger btn-xs">Update</button>
                                  </div>
                                  </form>
                              </div>
                          </div>
                      </div>