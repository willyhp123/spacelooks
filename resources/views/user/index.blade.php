@extends('user.layout.header')
@section('title','biodata User')
@section('content')
  <main class="main-content position-relative border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur" data-scroll="false">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-white active" aria-current="page">Biodata</li>
          </ol>
          <h6 class="font-weight-bolder text-white mb-0"></h6>
        </nav>
        
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="row">
             
              @include('user.request_validasi')
            </div>
           
            <div class="card-header pb-0">
              <img src="{{ asset('foto_user/'.$datas['foto_user']) }}" alt="" width="150px" height="150px" style="border-radius:100px;border:1px solid;margin-bottom:10px;">
              <h6>{{ $datas['username'] }}</h6>
              <p style="margin-top: -10px;">{{ $datas['email'] }}</p>
              <p style="margin-top: -20px"><small>{{ $datas['no_hp'] }}</small></p>
              @include('user.modal_edit.update_profile')
            </div>
            <div class="border"></div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="p-4">
                  <h5>Tentang</h2>
                  <p>{{ $datas['tentang'] }}
                  </p>
                  @include('user.modal_edit.tentang')
              </div>
              <div class="border"></div>
              <div class="p-4">
                <div class="row">
                  <div class="col">
                    <h5>Pendidikan</h5>
                  </div>
                  <div class="col" >
                    @include('user.modal.pendidikan')
                </div>
                  </div>
                </div>
                  @foreach ($pendidikan as $pendidikans)
                      <img src="{{ asset('foto_pendidikan/'.$pendidikans->foto_p) }}" alt="" width="100px" height="100px">
                      <h6>{{ $pendidikans->nama_pendidikan_p }}</h5>
                      <p style="margin-top:-10px"><small>{{ $pendidikans->nama_universitas_p }}</small></p>
                      <p style="margin-top:-20px;"><small>{{ $pendidikans->tanggal_awal_p }} - {{ $pendidikans->tanggal_akhir_p }}</small></p>
                      <p style="margin-top:-20px;"><small>{{ $pendidikans->domisili_p }}</small></p>
                      <div style="height:10px;"></div>
                     @include('user.modal_edit.pendidikan')
                     @include('user.modal_delete.pendidikan')
                      <br>
                  @endforeach
              </div>
            </div>
            <div class="border"></div>
              <div class="p-4">
                <div class = "row">
                  <div class="col">
                    <h5>Pengalaman Kerja</h5>
                  </div>
                  <div class="col">
                   @include('user.modal.pengalaman_kerja')
                  </div>
                </div>
                @foreach ($pengalaman_kerja as $pengalaman_kerjas)
                <img src="{{ asset('foto_pengalaman_kerja/'.$pengalaman_kerjas->foto_p_kerja) }}"
                 alt="" width="100px" height="100px">
                <h6>{{ $pengalaman_kerjas->nama_pengalaman_kerja }}</h6>
                <p style="margin-top:-10px"><small>{{ $pengalaman_kerjas->nama_perusahaan_p_kerja }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $pengalaman_kerjas->tanggal_mulai_p_kerja }} - {{ $pengalaman_kerjas->tanggal_akhir_p_kerja }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $pengalaman_kerjas->domisili_p_kerja }}</small></p>
                @include('user.modal_edit.pengalaman_kerja')
                @include('user.modal_delete.pengalaman_kerja')
                @endforeach
               
              </div>
              <div class="border"></div>
              <div class="p-4">
                <div class="row">
                  <div class="col">
                    <h5>Sertifikat</h5>
                  </div>
                  <div class="col">
                  @include('user.modal.sertifikat')
                  </div>
                </div>
               @foreach ($sertifikat as $sertifikats)
               <img src="{{ asset('foto_sertifikat/'.$sertifikats->foto_sertifikat) }}" 
               alt="" width="100px" height="100px">
               <h6>{{ $sertifikats->nama_sertifikat }}</h6>
               <p style="margin-top:-10px"><small>{{ $sertifikats->nama_perusahaan_sertifikat }}</small></p>
               <p style="margin-top:-20px;"><small>{{ $sertifikats->tanggal_awal_sertifikat }} - {{ $sertifikats ->tanggal_akhir_sertifikat }}</small></p>
               <p style="margin-top:-20px;"><small>{{ $sertifikats->domisili_sertifikat }}</small></p>
               @include('user.modal_edit.sertifikat')
               @include('user.modal_delete.sertifikat')
               @endforeach
            
              </div>
              <div class="border"></div>
              <div class="p-4">
                <div class="row">
                  <div class="col">
                    <h5>Pengalaman Kegiatan</h5>
                  </div>
                  <div class="col">
                    <!-- Button trigger modal -->
                      @include('user.modal.pengalaman_kegiatan')
                  </div>
                </div>   
                @foreach ($pengalaman_kegiatan as $pengalaman_kegiatans)
                <img src="{{ asset('foto_kegiatan/'.$pengalaman_kegiatans->foto_p_kegiatan) }}"
                 alt="" width="100px" height="100px">
                <h6>{{ $pengalaman_kegiatans->nama_kegiatan_p_kegiatan }}</h6>
                <p style="margin-top:-10px"><small>{{ $pengalaman_kegiatans->nama_perusahaan_p_kegiatan }}</small></p>
                <p style="margin-top:-20px;"><small>{{ $pengalaman_kegiatans->tanggal_awal_p_kegiatan }} - {{ $pengalaman_kegiatans->tanggal_akhir_p_kegiatan }} </small></p>
                <p style="margin-top:-20px;"><small>{{ $pengalaman_kegiatans->domisili_p_kegiatan }}</small></p>
                @include('user.modal_edit.pengalaman_kegiatan')
                @include('user.modal_delete.pengalaman_kegiatan')
                @endforeach
              </div>
              <div class="border"></div>
              <div class="p-4">
                <div class="row">
                  <div class="col">
                    <h5>Prestasi</h5>
                  </div>
                  <div class="col">
                    <!-- Button trigger modal -->
                      @include('user.modal.prestasi')
                  </div>
                </div>
          
               @foreach ($prestasi as $prestasis)
               <img src="{{ asset('foto_prestasi/'.$prestasis->foto_prestasi) }}" alt="" width="150px" height="150px">
               <h6>{{ $prestasis->nama_prestasi }}</h6>
               <p style="margin-top:-10px"><small>{{ $prestasis->nama_perusahaan_prestasi }}</small></p>
               <p style="margin-top:-20px;"><small>{{ $prestasis->tanggal_awal_prestasi }} - {{ $prestasis->tanggal_akhir_prestasi }}</small></p>
               <p style="margin-top:-20px;"><small>{{ $prestasis->domisili_prestasi }}</small></p>
               @include('user.modal_edit.prestasi') 
               @include('user.modal_delete.prestasi')
               <br>
               @endforeach
          
              </div>
          </div>
        </div>
      </div>
    
@endsection  
 
