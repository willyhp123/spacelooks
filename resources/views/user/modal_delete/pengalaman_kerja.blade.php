<a class="btn btn-warning btn-xs" data-toggle="modal" 
href='#m_pengalaman_kerja_delete-{{ $pengalaman_kerjas->id }}'><i class="fa fa-trash"></i></a>
<div class="modal fade" id="m_pengalaman_kerja_delete-{{ $pengalaman_kerjas->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Pengalaman Kerja</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{ route('delete_pengalaman_kerja',$pengalaman_kerjas->id) }}" method="post" enctype="multipart/form-data">
            @csrf
              <div class="modal-body">
              <h4>Apa Anda Yakin?</h2>
            </div>
            <div class="footer p-2">
              <button type="button" class="btn btn-outline-secondary btn-xs" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-danger btn-xs">Delete</button>
            </div>
            </form>
        </div>
    </div>
</div>