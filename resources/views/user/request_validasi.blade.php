@if (session()->get('success'))
<div class="col col-lg-3">
  <p class ="bg-success p-1 m-1" style="border-radius:5px;color:white;">{{ session()->get('success') }}</p>
</div>
@endif
@if (session()->get('fail'))
<div class="col col-lg-3">
<p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ session()->get('fail') }}</p>

</div>
@endif
@error('nama_pendidikan_p')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    
    </div>
@enderror
@error('nama_universitas_p')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    
    </div>
@enderror
@error('tanggal_awal_p')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    
    </div>
@enderror
@error('tanggal_akhir_p')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    
    </div>
@enderror
@error('domisili_p')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    
    </div>
@enderror
@error('nama_kegiatan_p_kegiatan')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    </div>
@enderror
@error('nama_perusahaan_p_kegiatan')
<div class="col col-lg-3">
    <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
    </div>
@enderror
@error('tanggal_awal_p_kegiatan')
 <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_akhir_p_kegiatan')
 <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('domisili_p_kegiatan')
<p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_pengalaman_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_perusahaan_p_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_mulai_p_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_akhir_p_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('domisili_p_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_perusahaan_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_awal_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_akhir_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('domisili_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('nama_perusahaan_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_awal_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('tanggal_akhir_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('domisili_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('foto_p')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('foto_p_kerja')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('foto_p_kegiatan')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('foto_prestasi')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror
@error('foto_sertifikat')
   <p class ="bg-danger  p-1 m-1" style="border-radius:5px;color:white;">{{ $message }}</p>
@enderror