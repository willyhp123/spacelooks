               <!-- Button trigger modal -->
               <button type="button" class="btn btn-outline-danger btn-xs" style="float: right;" data-toggle="modal" data-target="#pendidikan">
                <i class ="fa fa-plus"></i>
                </button>
        
        <!-- Modal -->
        <div class="modal fade" id="pendidikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pendidikan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ url('/tambah_pendidikan')}}" method="post" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <input type="text" name="user_id_p" value ="{{ $datas['id'] }}" hidden>
            <div class="form-control mb-2">
                <label for="">Foto</label>
                <input type="file" name ="foto_p" class ="form-control form-control-sm">
            </div>
          <div class ="form-control mb-2">
            <label for="">Nama Jurusan</label>
            <input type="text" name ="nama_pendidikan_p" class="form-control form-control-sm"  
            placeholder="Nama Pendidikan">
          </div>
          <div class ="form-control mb-2">
            <label for="">Nama Universitas</label>
            <input type="text" name ="nama_universitas_p" class="form-control form-control-sm" 
             placeholder="Nama Universitas">
          </div>
          <div class="form-control mb-2">
            <label for="">Tanggal awal</label>
            <input class="form-control form-control-sm" name ="tanggal_awal_p" type="date">
          </div>
          <div class ="form-control mb-2">
            <label for="">Tanggal Akhir</label>
            <input class="form-control form-control-sm" type="date" name ="tanggal_akhir_p" >
          </div>
          <div class="form-control mb-2">
            <label for="">Domisili</label>
            <input type="text" name ="domisili_p" class="form-control form-control-sm"  
            placeholder="Domisili">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Save</button>
        </div>
        </form>
        </div>
        </div>