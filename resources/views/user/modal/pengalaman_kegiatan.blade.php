<button type="button" class="btn btn-outline-danger btn-xs" style="float:right;" data-toggle="modal" data-target="#pengalaman_kegiatan">
    <i class="fa fa-plus"></i>
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="pengalaman_kegiatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pengalaman Kegiatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ url('/tambah_kegiatan') }}" method="post" enctype="multipart/form-data">
       @csrf
            <div class="modal-body">
              <input type="text" name="user_id_p_kegiatan" value ="{{ $datas['id'] }}" hidden>
                <div class="form-control mb-2">
                    <label for="">Foto</label>
                    <input type="file" name ="foto_p_kegiatan"
                     class ="form-control form-control-sm @error('foto_p_kegiatan')
                     is-invalid @enderror">
                     @error('foto_p_kegiatan')
                     <span class ="invalid-feedback">{{$message}}</span>
                     @enderror
                </div>
          <div class="form-control mb-2">
            <label for="">Nama Kegiatan</label>
            <input type="text" name ="nama_kegiatan_p_kegiatan" 
            class ="form-control form-control-sm @error('nama_kegiatan_p_kegiatan') is-invalid @enderror"
            placeholder="Nama Kegiatan">
            @error('nama_kegiatan_p_kegiatan')
            <span class ="invalid-feedback">{{$message}}</span>
            @enderror
          </div>
          <div class="form-control mb-2">
            <label for="">Nama Perusahaan</label>
            <input type="text" name="nama_perusahaan_p_kegiatan" 
            class ="form-control form-coontrol-sm @error('nama_perusahaan_p_kegiatan') is-invalid @enderror"
            placeholder="Nama Perusahaan">
            @error('nama_perusahaan_p_kegiatan')
            <span class ="invalid-feedback">{{$message}}</span>
            @enderror
          </div>
          <div class="form-control mb-2">
            <label for="">Tanggal Awal</label>
            <input type="date" name ="tanggal_awal_p_kegiatan" 
            class ="form-control form-control-sm @error('tanggal_awal_p_kegiatan') is-invalid @enderror">
            @error('tanggal_awal_p_kegiatan')
            <span class ="invalid-feedback">{{$message}}</span>
            @enderror
          </div>
          <div class="form-control mb-2">
            <label for="">Tanggal Akhir</label>
            <input type="date" name ="tanggal_akhir_p_kegiatan" 
            class ="form-control form-control-sm @error('tanggal_akhir_p_kegiatan') is-invalid @enderror">
            @error('tanggal_akhir_p_kegiatan')
            <span class ="invalid-feedback">{{$message}}</span>
            @enderror
          </div>
          <div class="form-control mb-2">
            <label for="">Domisili</label>
            <input type="text" name ="domisili_p_kegiatan" 
            class ="form-control form-control-sm @error('domisili_p_kegiatan') is-invalid @enderror"
            placeholder="Domisili">
            @error('domisili_p_kegiatan')
            <span class ="invalid-feedback">{{$message}}</span>
            @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>