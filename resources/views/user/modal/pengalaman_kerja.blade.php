 <!-- Button trigger modal -->
 <button type="button" class="btn btn-outline-danger btn-xs" style="float:right;"
 data-toggle="modal" data-target="#pengalaman_kerja">
  <i class ="fa fa-plus"></i>
</button>
<!-- Modal -->
<div class="modal fade" id="pengalaman_kerja" tabindex="-1" role="dialog"
 aria-labelledby="pengalaman_kerja" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pengalaman kerja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('/tambah_pengalaman_kerja')}}" method="POST" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          <input type="text" name="user_id_p_kerja" value ="{{ $datas['id'] }}" hidden>
            <div class="form-control mb-2">
                <label for="">Foto</label>
                <input type="file" name ="foto_p_kerja" 
                class ="form-control form-control-sm @error('foto_p_kerja') is-invalid @enderror">
                @error('foto_p_kerja')
                <span class ="invalid-feedback">{{$message}}</span>
                @enderror
            </div>
        <div class="form-control mb-2">
          <label for="">Nama Pengalaman Kerja</label>
          <input type="text" name ="nama_pengalaman_kerja" 
          class="form-control form-control-sm @error('nama_pengalaman_kerja') is-invalid @enderror"  
          placeholder="Nama Universitas">
          @error('nama_pengalaman_kerja')
          <span class ="invalid-feedback">{{$message}}</span>
          @enderror
        </div>
        <div class="form-control mb-2">
          <label for="">Nama Perusahaan</label>
          <input type="text" name="nama_perusahaan_p_kerja" 
          class ="form-control form-control-sm @error('nama_perusahaan_p_kerja') is-invalid @enderror"
          placeholder="Nama Perusahaan">
          @error('nama_perusahaan_p_kerja')
          <span class ="invalid-feedback">{{$message}}</span>
          @enderror
        </div>
        <div class="form-control mb-2">
          <label for="">tanggal Mulai</label>
          <input type="date" name ="tanggal_mulai_p_kerja" 
          class="form-control form-control-sm @error('tanggal_mulai_p_kerja') is-invalid @enderror"  
          >
          @error('tanggal_mulai_p_kerja')
           <span class ="invalid-feedback">{{$message}}</span>
          @enderror
        </div>
        <div class="form-control mb-2">
          <label for="">Tanggal Akhir</label>
          <input type="date" name ="tanggal_akhir_p_kerja" 
          class ="form-control form-control-sm @error('tanggal_akhir_p_kerja') is-invalid @enderror">
          @error('tanggal_akhir_p_kerja')
          <span class ="invalid-feedback">{{$message}}</span>
         @enderror
        </div>
        <div class="form-control mb-2">
          <label for="">Domisili</label>
          <input type="text" name ="domisili_p_kerja" placeholder="Domisili" 
          class ="form-control form-control-sm @error('domisili_p_kerja') is-invalid @enderror">
          @error('domisili_p_kerja')
          <span class ="invalid-feedback">{{$message}}</span>
         @enderror
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>