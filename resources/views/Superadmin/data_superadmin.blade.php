@extends('Superadmin.layouts.header')

@section('title','Data Superadmin')

@section('content')
<div class="pcoded-content">
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Dashboard</h5>
                        <p class="m-b-0">Data Superadmin</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html"> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Superadmin</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
      <div class="pcoded-inner-content">
          <!-- Main-body start -->
          <div class="main-body">
              <div class="page-wrapper">
                  <!-- Page-body start -->
                  <div class="page-body">
                      <div class="card">
                          <div class="card-header">
                             <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
    <i class="fa fa-plus"></i> Tambah
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('tambah_data_superadmin') }}" method="post" >
            @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="">Username</label>
                <input type="text" name="username" class ="form-control form-control-sm" placeholder="Username">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                <input type="password" class="form-control form-control-sm" name = "password" placeholder="Password">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>
                              <div class="card-header-right">
                                  <ul class="list-unstyled card-option">
                                      <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                      <li><i class="fa fa-window-maximize full-card"></i></li>
                                      <li><i class="fa fa-minus minimize-card"></i></li>
                                      <li><i class="fa fa-refresh reload-card"></i></li>
                                      <li><i class="fa fa-trash close-card"></i></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="card-block table-border-style">
                              <div class="table-responsive">
                                  <form action="" method="post">
                                      <div class="form-group">
                                          <div class ="row">
                                              <div class ="col col-lg-4">
                                                  <input type="text" name="" class ="form-control form-control-sm" placeholder="Email">
                                              </div>
                                              <div class ="col col-lg-8">
                                               
                                              </div>
                                          </div>
                                         
                                        
                                       </div>
                                       <div class ="form-group">
                                          <button type="submit" class ="btn btn-primary btn-sm">Filter</button>
                                       </div>
                                  </form>
                                  <table class="table">
                                      <thead>
                                          <tr>
                                              <th>No</th>
                                              <th>Username</th>
    
                                           
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach ($superadmin as $key=>$val)
                                              <tr>
                                                  <td>{{ $key+1 }}</td>
                                                  <td>{{ $val->username }}</td>
                                                  
                                              </tr>
                                          @endforeach
                                       
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- Page-body end -->
              </div>
              <div id="styleSelector"> </div>
          </div>
      </div>
  </div>
</div>
</div>
</div>
</div>
@endsection