@extends('Superadmin.layouts.header')

@section('title','User Portofolio')

@section('content')
     <div class="pcoded-content">
                      <!-- Page-header start -->
                      <div class="page-header">
                          <div class="page-block">
                              <div class="row align-items-center">
                                  <div class="col-md-8">
                                      <div class="page-header-title">
                                          <h5 class="m-b-10">Dashboard</h5>
                                          <p class="m-b-0">Portofolio User</p>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <ul class="breadcrumb-title">
                                          <li class="breadcrumb-item">
                                              <a href="index.html"> <i class="fa fa-home"></i> </a>
                                          </li>
                                          <li class="breadcrumb-item"><a href="#!">Data User</a>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>User Portofolio</h5>
                                                <span>User Yang Sudah Register</span>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    <i class="fa fa-plus"></i>
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
                                                    <form action="" method="post">
                                                        <div class="form-group">
                                                            <div class ="row">
                                                                <div class ="col col-lg-4">
                                                                    <input type="text" name="" class ="form-control form-control-sm" placeholder="Email">
                                                                </div>
                                                                <div class ="col col-lg-8">
                                                                 
                                                                </div>
                                                            </div>
                                                         </div>
                                                         <div class ="form-group">
                                                            <button type="submit" class ="btn btn-primary btn-sm">Filter</button>
                                                         </div>
                                                    </form>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Username</th>
                                                                <th>Email</th>
                                                                <th>No hp</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           @foreach ($users as $key=>$val)
                                                               <tr>
                                                                   <th>{{ ++$key }}</th>
                                                                   <th>{{ $val->username }}</th>
                                                                   <th>{{ $val->email }}</th>
                                                                   <th>{{ $val->no_hp }}</th>
                                                                   <th>
                                                                    <a class="btn btn-sm" data-toggle="modal" style="color:orange;" href='#m_edit-{{ $val->id }}'><i class="fa fa-edit" style="margin: -100px;font-size:15px;"></i></a>
                                                                    <div class="modal fade" id="m_edit-{{ $val->id }}">
                                                                        <div class="modal-dialog modal-sm">
                                                                            
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h4 class="modal-title">Edit User</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                </div>
                                                                                <form action="{{ route('edit_profile',$val->id) }}" method="post" enctype="multipart/form-data">
                                                                                    @csrf
                                                                                <div class="modal-body">
                                                                                    <div class ="form-control">
                                                                                        <label for="">Foto</label><br>
                                                                                        <img src="{{ asset('foto_user/'.$val->foto_user) }}" width="100px" height="100px">
                                                                                        <div style="height: 10px;"></div>
                                                                                        <input type="file" name ="foto_user" class ="form-control form-control-sm" 
                                                                                         value ="Foto User">
                                                                                    </div>
                                                                                        <div class ="form-group">
                                                                                            <label for="">Username</label>
                                                                                            <input type="text" name ="username" class ="form-control form-control-sm" 
                                                                                             value ="{{ $val->username }}">
                                                                                        </div>
                                                                                       
                                                                                        <div class ="form-group">
                                                                                            <label for="">No Hp</label>
                                                                                            <input type="text" name ="no_hp" class ="form-control form-control-sm" 
                                                                                             value ="{{ $val->no_hp }}">
                                                                                        </div>

                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-default btn-sm" style="border-radius: 5px;" data-dismiss="modal">Close</button>
                                                                                            <button type="submit" name = "submit" class="btn btn-warning btn-sm "style="border-radius: 5px;"><i class = "fa fa-edit"></i> Update</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a class="btn btn-sm" data-toggle="modal" style="color:red;" href='#m_delete-{{ $val->id }}'><i class="fa fa-trash" style="margin: -100px;font-size:15px;"></i></a>
                                                                    <div class="modal fade" id="m_delete-{{ $val->id }}">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h6 class="modal-title">Delete {{ $val->username }}</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                </div>
                                                                                <form action="{{ route('delete_profile',$val->id) }}" method="post">
                                                                                    @csrf
                                                                                <div class="modal-body">
                                                                                  <h5>Apa Anda Yakin?</h5>
                                                                                      
                                                                                  </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                                                            <button type="submit" name = "submit" class="btn btn-danger btn-sm"><i class = "fa fa-trash"></i> Delete</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a class="btn btn-sm"  style="color:grey;" href="{{ route('biodata_user',$val->id) }}"><i class="fa fa-eye" style="margin: -100px;font-size:15px;"></i></a>
                                                                   
                                                                   </th>
                                                               </tr>
                                                           @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection