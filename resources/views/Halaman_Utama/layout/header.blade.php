
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('admin_spacelook/assets/img/favicon.png') }}">
  <title>
    @yield('title')
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('admin_spacelook/assets/css/nucleo-icons.css') }}" rel="stylesheet" />
  <link href=".{{ asset('admin_spacelook/assets/css/nucleo-svg.css') }}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="{{ asset('admin_spacelook/assets/css/nucleo-svg.css') }}" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('admin_spacelook/assets/css/argon-dashboard.css?v=2.0.1') }}"
   rel="stylesheet" />
</head>
<body class="g-sidenav-show   bg-gray-100">
  <div class="min-height-300 bg-primary position-absolute w-100"></div>
 
  <main class="main-content position-relative border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur" data-scroll="false">
      <div class="container-fluid py-1 px-3">
        
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <ul class="navbar-nav">
              <li class="nav-item d-flex pr-2">
                <a href="{{ route('biodata') }}" class="nav-link text-white font-weight-bold px-3">
                  <i class="fa fa-refresh me-sm-1"></i>
                  <span class="d-sm-inline d-none">Refresh</span>
                </a>
              </li>
            </ul>
            <form action="{{ route('biodata_search') }}" method="post">
              @csrf
            <div class="input-group">
              <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
              <input type="text" name="search" class="form-control" placeholder="Type here...">
            </div>
            </form>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="{{ route('login') }}" class="nav-link text-white font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">Login</span>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="{{ route('register') }}" class="nav-link text-white font-weight-bold px-0">
                <i class="fa fa-user me-sm-1 mr-2"></i>
                <span class="d-sm-inline d-none">Register</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
@yield('content')
@include('Halaman_Utama.layout.footer')
