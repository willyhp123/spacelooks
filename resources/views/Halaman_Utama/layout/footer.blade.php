<footer class="footer pt-3  ">
    <div class="container-fluid">
      <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6 mb-lg-0 mb-4">
          <div class="copyright text-center text-sm text-muted text-lg-start">
            
           
           
          </div>
        </div>
        <div class="col-lg-6">
         
        </div>
      </div>
    </div>
  </footer>
  </div>
  </main>
   <!--   Core JS Files   -->
   <script src="{{ asset('admin_spacelook/assets-old/vendor/jquery/dist/jquery.min.js') }}"></script>
   <script src="{{asset('admin_spacelook/assets/js/core/popper.min.js') }}"></script>
   <script src="{{ asset('admin/spacelook/assets/js/core/bootstrap.min.js') }}"></script>
   <script src="{{ asset('admin_spacelook/assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
   <script src="{{ asset('admin_spacelook/assets/js/plugins/smooth-scrollbar.min.js') }}"></script>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <script>
     var win = navigator.platform.indexOf('Win') > -1;
     if (win && document.querySelector('#sidenav-scrollbar')) {
       var options = {
         damping: '0.5'
       }
       Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
     }
   </script>
   <!-- Github buttons -->
   <script async defer src="https://buttons.github.io/buttons.js"></script>
   <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
   <script src="{{ asset('admin_spacelook/assets/js/argon-dashboard.min.js?v=2.0.1') }}"></script>
  </body>
  
  </html>