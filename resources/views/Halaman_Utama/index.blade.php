
@extends('Halaman_Utama.layout.header')

@section('title','Portofolio')

@section('content')

    <div class="container-fluid py-4">
      <div class="row">
      
          @foreach ($users as $val)
          <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div style="height: 15px;"></div>
            <img class="card-img-top" src="{{ asset('foto_user/'.$val->foto_user) }}"  
            style="margin:auto;width:150px;height:150px;border:3px solid white;border-radius:100px;">
            <div class="card-body">
                <h6 class="card-title" style="margin-bottom: -3px;"><center>{{ $val->username }}</center></h6>
                <b class="card-text" style="margin-top: -10px;"><center><small>{{ $val->no_hp }}</small></center></b>
                <small style="font-size: 10px;"><center>{{ $val->email }}</center></small>
                <div style="height: 10px;"></div>
              <center><a href="{{ route('biodata_user',$val->id) }}" class="btn btn-primary btn-xs">More Detail</a></center> 
            </div>
              </div>
          </div>
          @endforeach
      </div>
      
   
 
  <!--   Core JS Files   -->
  <script src="{{ asset('admin_spacelook/assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('admin_spacelook/assets/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin_spacelook/assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('admin_spacelook/assets/js/plugins/smooth-scrollbar.min.js') }}"></script>
  <script src="{{ asset('admin_spacelook/assets/js/plugins/chartjs.min.js') }}"></script>
  <script>
    var ctx1 = document.getElementById("chart-line").getContext("2d");

    var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(94, 114, 228, 0.2)');
    gradientStroke1.addColorStop(0.2, 'rgba(94, 114, 228, 0.0)');
    gradientStroke1.addColorStop(0, 'rgba(94, 114, 228, 0)');
    new Chart(ctx1, {
      type: "line",
      data: {
        labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Mobile apps",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          borderColor: "#5e72e4",
          backgroundColor: gradientStroke1,
          borderWidth: 3,
          fill: true,
          data: [50, 40, 300, 220, 500, 250, 400, 230, 500],
          maxBarThickness: 6

        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#fbfbfb',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#ccc',
              padding: 20,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
  </script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('admin_spacelook/assets/js/argon-dashboard.min.js?v=2.0.1') }}"></script>
</body>
@endsection

