<?php

use App\Http\Controllers\ViewController;
use GuzzleHttp\Middleware;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/biodata/{id}',[ViewController::class,'biodata'])->name('biodata_user');
route::post('/biodata/search',[ViewController::class,'biodata_search'])->name('biodata_search');
route::get('/logins',[ViewController::class,'login'])->name('login')->middleware('loginalready');
route::post('/check',[ViewController::class,'check'])->name('check');
route::get('/',[ViewController::class,'index'])->name('biodata');
route::get('/register',[ViewController::class,'register'])->name('register')->middleware('loginalready');
route::post('/register_user',[ViewController::class,'register_user'])->name('register_user');
route::get('/logout',[ViewController::class,'logout'])->name('logout');
route::get('/logout_superadmin',[ViewController::class,'logout_superadmin'])->name('logout_superadmin');
Route::group(['middleware' => ['authcheck']], function () {
    route::get('/user',[ViewController::class,'user'])->name('user');
    route::post('/tambah_pendidikan',[ViewController::class,'add_pendidikan'])
    ->name('tambah_pendidikan');
    route::post('/tambah_kegiatan',[ViewController::class,'add_pengalaman_kegiatan'])
    ->name('tambah_pengalaman_kegiatan');
    route::post('/tambah_pengalaman_kerja',[ViewController::class,'add_pengalaman_kerja'])
    ->name('tambah_pengalaman_kerja');
    route::post('/tambah_sertifikat',[ViewController::class,'add_sertifikat'])
    ->name('tambah_sertifikat');
    route::post('/tambah_prestasi',[ViewController::class,'add_prestasi'])
    ->name('tambah_prestasi');
    route::post('update_pendidikan/{id}',[ViewController::class,'update_pendidikan'])
    ->name('update_pendidikan');
    route::post('update_kegiatan/{id}',[ViewController::class,'update_pengalaman_kegiatan'])
    ->name('update_kegiatan');
    route::post('update_pengalaman_kerja/{id}',[ViewController::class,'update_pengalaman_kerja'])
    ->name('update_pengalaman_kerja');
    route::post('update_prestasi/{id}',[ViewController::class,'update_prestasi'])
    ->name('update_prestasi');
    route::post('update_sertifikat/{id}',[ViewController::class,'update_sertifikat'])
    ->name('update_sertifikat');
    route::post('delete_pendidikan/{id_p}',[ViewController::class,'delete_pendidikan'])
    ->name('delete_pendidikan');
    route::post('delete_pengalaman_kerja/{id}',[ViewController::class,'delete_pengalaman_kerja'])
    ->name('delete_pengalaman_kerja');
    route::post('delete_pengalaman_kegiatan/{id}',[ViewController::class,'delete_pengalaman_kegiatan'])
    ->name('delete_pengalaman_kegiatan');
    route::post('delete_prestasi/{id}',[ViewController::class,'delete_prestasi'])
    ->name('delete_prestasi');
    route::post('delete_sertifikat/{id}',[ViewController::class,'delete_sertifikat'])
    ->name('delete_sertifikat');
    route::post('update_tentang/{id}',[ViewController::class,'edit_tentang'])
    ->name('update_tentang');
    route::post('update_profile/{id}',[ViewController::class,'edit_profile'])
    ->name('edit_profile');
    route::post('update_profile/{id}',[ViewController::class,'edit_profile'])
    ->name('edit_profile');
    route::post('delete_profile/{id}',[ViewController::class,'delete_profile'])
    ->name('delete_profile');
});
Route::group(['middleware' => ['authsuperadmin']], function () {
    route::get('/superadmin',[ViewController::class,'superadmin'])->name('superadmin');
    route::get('/superadmin/data_user',[ViewController::class,'superadmin_user'])
    ->name('superadmin_user');
    route::get('superadmin/data',[ViewController::class,'data_superadmin'])->name('data_superadmin');
    route::post('superadmin/tambah_data',[ViewController::class,'tambah_data_superadmin'])
    ->name('tambah_data_superadmin');
});
route::post('/proses_login_administrator',[ViewController::class,'proses_login_administrator'])
->name('proses_login_administrator');
route::get('/administrator',[ViewController::class,'login_superadmin'])
->name('login_superadmin')->middleware('already');





