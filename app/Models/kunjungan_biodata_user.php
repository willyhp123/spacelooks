<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kunjungan_biodata_user extends Model
{
    protected $table = "kunjungan_biodata_users";
    protected $fillable = [
        'jumlah_kunjungan'
    ];
}
