<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengalaman_kerja extends Model
{
   protected $table = 'pengalaman_kerjas';
   protected $primarykey = 'id';
   protected $fillable = [
        'nama_pengalaman_kerja','nama_perusahaan_p_kerja','tanggal_mulai_p_kerja',
        'tanggal_akhir_p_kerja','domisili_p_kerja','user_id_p_kerja'
   ];

}
