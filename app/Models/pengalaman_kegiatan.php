<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengalaman_kegiatan extends Model
{
    protected $table = 'pengalaman_kegiatans';
    protected $fillable = [
        'nama_kegiatan','nama_perusahaan','tanggal_awal','tanggal_akhir','domisili'
    ];
}
