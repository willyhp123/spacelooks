<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class prestasi extends Model
{
    protected $table = 'prestasis';
    protected $fillable = [
        'nama_prestasi','nama_perusahaan_prestasi','tanggal_awal_prestasi','tanggal_akhir_prestasi',
        'domisili_prestasi','foto_prestasi','user_id_prestasi','id'
    ];
}
