<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kunjungan_halaman_admin_user extends Model
{
    protected $table = "kunjungan_halaman_admin_users";
    protected $fillable = [
        'jumlah_kunjungan'
    ];
}
