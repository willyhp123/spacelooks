<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class superadmin extends Model
{
    protected $table = "superadmins";
    protected $fillable = [
        'username','password'
    ];
}
