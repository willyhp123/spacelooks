<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_pengguna extends Model
{
   protected $table = 'user_penggunas';
   protected $fillable =[
       'username','email','no_hp','email','password','id_pendidikan','id_sertifikat',
       'id_pengalaman_kerja','id_pengalaman_kegiatan','id_prestasi','foto_user'
   ];
}
