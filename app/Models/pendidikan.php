<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pendidikan extends Model
{
    protected $table = 'pendidikans';
    protected $primarykey = 'id_p';
    protected $fillable = [
        'nama_pendidikan_p','nama_universitas_p','tanggal_awal_p',
        'tanggal_akhir_p',
        'domisili_p','user_id_p'
    ];
}
