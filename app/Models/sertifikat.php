<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sertifikat extends Model
{
    protected $table = 'sertifikats';
    protected $fillable = [
        'nama_sertifikat','nama_perusahaan_sertifikat','tanggal_awal_sertifikat','tanggal_akhir_sertifikat'
        ,'domisili_sertifikat','user_id_sertifikat'
    ];
}
