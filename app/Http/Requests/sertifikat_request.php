<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sertifikat_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_sertifikat'=>'required',
            'nama_perusahaan_sertifikat' => 'required',
            'tanggal_awal_sertifikat' =>'required',
            'tanggal_akhir_sertifikat' =>'required',
            'domisili_sertifikat' => 'required'
        ];
    }
    public function messages(){
        return[
            'nama_sertifikat.required' => 'Nama Prestasi Tidak Boleh Kosong',
            'nama_perusahaan_sertifikat.required'=> 'Nama Perusahaan(sertifikat) Tidak Boleh kosong',
            'tanggal_mulai_sertifikat.required' => 'Tanggal Mulai(sertifikat) Tidak Boleh Kosong',
            'tanggal_akhir_sertifikat.required' => 'Tanggal Akhir(sertifikat) Tidak Boleh kosong',
            'domisili_sertifikat.required' => 'Domisili(sertifikat) Tidak Boleh Kosong'
        ];
    }
}
