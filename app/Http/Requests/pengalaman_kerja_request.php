<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class pengalaman_kerja_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pengalaman_kerja'=>'required',
            'nama_perusahaan_p_kerja' =>'required',
            'tanggal_mulai_p_kerja' =>'required',
            'tanggal_akhir_p_kerja'=>'required',
            'domisili_p_kerja' => 'required',
            'foto_p_kerja' => 'required'
        ];
    }
    public function messages(){
        return[
            'nama_pengalaman_kerja.required' => 'Nama Pengalaman Kerja Tidak Boleh Kosong',
            'nama_perusahaan_p_kerja.required'=> 'Nama Perusahaan(Pengalaman Kerja) Tidak Boleh kosong',
            'tanggal_mulai_p_kerja.required' => 'Tanggal Mulai(Pengalaman Kerja) Tidak Boleh Kosong',
            'tanggal_akhir_p_kerja.required' => 'Tanggal Akhir(Pengalaman Kerja) Tidak Boleh kosong',
            'domisili_p_kerja.required' => 'Domisili(Pengalaman Kerja) Tidak Boleh Kosong'
        ];
    }
}
