<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class pengalaman_kegiatan_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_kegiatan_p_kegiatan'   =>'required',
            'nama_perusahaan_p_kegiatan' =>'required',
            'tanggal_awal_p_kegiatan'    =>'required',
            'tanggal_akhir_p_kegiatan'   =>'required',
            'domisili_p_kegiatan' => 'required'

        ];
    }
    public function messages(){
        return[
            'nama_kegiatan_p_kegiatan.required' => 'Nama Kegiatan Tidak Boleh Kosong',
            'nama_perusahaan_p_kegiatan.required' => 'Nama Perusahaan (Pengalaman Kegiatan) Tidak Boleh Kosong',
            'tanggal_awal_p_kegiatan.required' => 'Tanggal awal (Pengalaman Kegiatan) tidak Boleh Kosong',
            'tanggal_akhir_p_kegiatan.required' =>'Tanggal akhir (Pengalaman Kegiatan) Tidak Boleh Kosong',
            'Domisili_p_kegiatan' =>'Domisili (pengalaman Kegiatan) Tidak Boleh Kosong'
        ];
     
    }
}
