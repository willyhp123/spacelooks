<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class pendidikan_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pendidikan_p' => 'required',
            'nama_universitas_p'=> 'required',
            'tanggal_awal_p'    => 'required',
            'tanggal_akhir_p'   => 'required',
            'domisili_p'        => 'required'

        ];
    }
    public function messages(){
        return[
            'nama_pendidikan_p.required'  => 'Nama Pendidikan Tidak boleh Kosong',
            'nama_universitas_p.required' => 'Nama Universitas Tidak Boleh Kosong',
            'tanggal_awal_p.required'     => 'tanggal_awal Tidak Boleh Kosong',
            'tanggal_akhir_p.required'    => 'tanggal_akhir Tidak Boleh Kosong',
            'domisili_p.required'         => 'Domisili Tidak Boleh Kosong'
        ];
    }
}
