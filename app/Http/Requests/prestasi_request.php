<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class prestasi_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_prestasi'=>'required',
            'nama_perusahaan_prestasi' => 'required',
            'tanggal_awal_prestasi' =>'required',
            'tanggal_akhir_prestasi' =>'required',
            'domisili_prestasi' => 'required'
        ];
    }
    public function messages(){
        return[
            'nama_prestasi.required' => 'Nama Prestasi Tidak Boleh Kosong',
            'nama_perusahaan_prestasi.required'=> 'Nama Perusahaan(Prestasi) Tidak Boleh kosong',
            'tanggal_mulai_prestasi.required' => 'Tanggal Mulai(Prestasi) Tidak Boleh Kosong',
            'tanggal_akhir_prestasi.required' => 'Tanggal Akhir(Prestasi) Tidak Boleh kosong',
            'domisili_prestasi.required' => 'Domisili(Prestasi) Tidak Boleh Kosong'
        ];
    }
}
