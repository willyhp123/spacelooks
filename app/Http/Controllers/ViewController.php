<?php

namespace App\Http\Controllers;

use App\Http\Requests\pendidikan_request;
use App\Http\Requests\pengalaman_kegiatan_request;
use App\Http\Requests\pengalaman_kerja_request;
use App\Http\Requests\prestasi_request;
use App\Http\Requests\sertifikat_request;
use App\Models\kunjungan_biodata_user;
use App\Models\kunjungan_halaman_admin_user;
use App\Models\kunjungan_halaman_utama;
use App\Models\pendidikan;
use App\Models\pengalaman_kegiatan;
use App\Models\pengalaman_kerja;
use App\Models\prestasi;
use App\Models\sertifikat;
use App\Models\superadmin;
use App\Models\User;
use App\Models\user_pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;


class ViewController extends Controller
{
    public function index(Request $request){
        $kunjungan_halaman_utama = kunjungan_halaman_utama::find(1);
        $kunjungan_halaman_utama->jumlah_kunjungan+=1;
        $kunjungan_halaman_utama->update();
        $users = DB::table('user_penggunas')
                ->select('username','email','no_hp','foto_user','id')
                ->get();
                return view('Halaman_Utama.index',['users'=>$users]);
    }
    public function biodata_search(Request $request){
        $users = DB::table('user_penggunas')
        ->select('username','email','no_hp','foto_user','id')
        ->where('username','=',$request->search)
        ->orwhere('email','=',$request->search)
        ->orwhere('no_hp','=',$request->search)
        ->get();
        if($request->search == ""){
            $users = DB::table('user_penggunas')
            ->select('username','email','no_hp','foto_user','id')
            ->get();
            return view('Halaman_Utama.index',['users'=>$users]);
        }
        return view('Halaman_Utama.index',['users'=>$users]);
       
    }
    public function login(){
        return view('login.index');
    }
    public function user(){
        $biodata_user = kunjungan_biodata_user::find(1);
        $biodata_user->jumlah_kunjungan += 1;
        $biodata_user->update();
        $data =["datas" => user_pengguna::where('id','=',session('loginID'))->first()]; 
        $pendidikan = DB::table('pendidikans')
                        ->select('nama_pendidikan_p','nama_universitas_p',
                        'tanggal_awal_p','tanggal_akhir_p','domisili_p','id','foto_p')
                        ->where('user_id_p','=',session('loginID'))
                        ->get();

        $pengalaman_kerja =DB::table('pengalaman_kerjas')
                              ->select('nama_pengalaman_kerja','nama_perusahaan_p_kerja',
                              'tanggal_mulai_p_kerja','tanggal_akhir_p_kerja','domisili_p_kerja'
                              ,'id','foto_p_kerja','foto_p_kerja')
                              ->where('user_id_p_kerja','=',session('loginID'))
                              ->get();

        $pengalaman_kegiatan = DB::table('pengalaman_kegiatans')
                                 ->select('nama_kegiatan_p_kegiatan','nama_perusahaan_p_kegiatan',
                                   'tanggal_awal_p_kegiatan','tanggal_akhir_p_kegiatan',
                                   'domisili_p_kegiatan','id','foto_p_kegiatan')
                                 ->where('user_id_p_kegiatan','=',session('loginID'))
                                 ->get();

        $prestasi = DB::table('prestasis')
                      ->select('nama_prestasi','nama_perusahaan_prestasi','tanggal_awal_prestasi',
                      'tanggal_akhir_prestasi','domisili_prestasi','id','foto_prestasi')
                      ->where('user_id_prestasi','=',session('loginID'))
                      ->get();

        $sertifikat = DB::table('sertifikats')
                        ->select('nama_sertifikat','nama_perusahaan_sertifikat','tanggal_awal_sertifikat',
                        'tanggal_akhir_sertifikat','domisili_sertifikat','id','foto_sertifikat')
                        ->where('user_id_sertifikat','=',session('loginID'))
                        ->get();

                    return view('user.index',$data,['pendidikan' => $pendidikan,
                    'pengalaman_kerja' => $pengalaman_kerja,'pengalaman_kegiatan'=> $pengalaman_kegiatan,
                    'prestasi' => $prestasi,'sertifikat'=> $sertifikat
                ]);

    }
    public function biodata($id){
        $user_detail = ["datas" => user_pengguna::where('id','=',$id)->first()];

        $pendidikan = DB::table('pendidikans')
                     ->select('nama_pendidikan_p','nama_universitas_p',
                     'tanggal_awal_p','tanggal_akhir_p','domisili_p','id','foto_p')
                     ->where('user_id_p','=',$id)
                     ->get();

     $pengalaman_kerja =DB::table('pengalaman_kerjas')
                           ->select('nama_pengalaman_kerja','nama_perusahaan_p_kerja',
                           'tanggal_mulai_p_kerja','tanggal_akhir_p_kerja','domisili_p_kerja'
                           ,'id','foto_p_kerja','foto_p_kerja')
                           ->where('user_id_p_kerja','=',$id)
                           ->get();

     $pengalaman_kegiatan = DB::table('pengalaman_kegiatans')
                              ->select('nama_kegiatan_p_kegiatan','nama_perusahaan_p_kegiatan',
                                'tanggal_awal_p_kegiatan','tanggal_akhir_p_kegiatan',
                                'domisili_p_kegiatan','id','foto_p_kegiatan')
                              ->where('user_id_p_kegiatan','=',$id)
                              ->get();

     $prestasi = DB::table('prestasis')
                   ->select('nama_prestasi','nama_perusahaan_prestasi','tanggal_awal_prestasi',
                   'tanggal_akhir_prestasi','domisili_prestasi','id','foto_prestasi')
                   ->where('user_id_prestasi','=',$id)
                   ->get();

     $sertifikat = DB::table('sertifikats')
                     ->select('nama_sertifikat','nama_perusahaan_sertifikat','tanggal_awal_sertifikat',
                     'tanggal_akhir_sertifikat','domisili_sertifikat','id','foto_sertifikat')
                     ->where('user_id_sertifikat','=',$id)
                     ->get();
        return view('Biodata.index',$user_detail,[
                     'pendidikan'=>$pendidikan,
                     'pengalaman_kerja'=>$pengalaman_kerja,
                     'pengalaman_kegiatan'=>$pengalaman_kegiatan,
                     'prestasi'=>$prestasi,
                     'sertifikat'=>$sertifikat
    ]);

    }
    public function register(){
        return view('login.register');
    }
    public function check(Request $request){
        request()->validate([
            'username'=>'required',
            'password'=>'required',
        ]);
        $user = user_pengguna::where('username','=',$request->username)->first();
        if($user){
            if(Hash::check($request->password,$user->password)){
                $request->session()->put('loginID',$user->id);
                $kunjungan_admin = kunjungan_halaman_admin_user::find(1)->first();
                $kunjungan_admin->jumlah_kunjungan += 1;
                $kunjungan_admin->update();
                return redirect('/user'); 
            }
            else{
                return redirect()->back()->with('fail','password Anda salah');
            }
        }else{
            return redirect()->back()->with('fail','Username Tidak Terdaftar');
        }        
    }
    public function register_user(Request $request){
        if($request->password == $request->confirm_password){
            $user = new user_pengguna();
            $user->username = $request->username;
            $user->email = $request->email;
            $user->no_hp = $request->no_hp;
            $user->password = Hash::make($request->password);
            if ($request->hasFile('foto_user')) //Cek apakah ada file avatar yang di Upload
            {
                $request->file('foto_user')->move('foto_user/', 
                $request->file('foto_user')
                ->getClientOriginalName());
                $user->foto_user = $request->file('foto_user')
                ->getClientOriginalName();
                $user->save();
               
            }
            return redirect()->back()->with('success','Data Berhasil Input');
        }
        else{
            return redirect()->back()->with('fail','password tidak sama dengan Confirm Password');
        }
      
    }
    public function logout(){
        if(Session::has('loginID')){
            Session::pull('loginID');
            Return redirect('logins');
        }
    }
    public function logout_superadmin(){
        if(Session::has('superadminID')){
            Session::pull('superadminID');
            Return redirect('administrator');
        }
    }

   public function add_pendidikan(pendidikan_request $request){
       $pendidikan = new pendidikan();
       $pendidikan->nama_pendidikan_p = $request->nama_pendidikan_p;
       $pendidikan->nama_universitas_p = $request->nama_universitas_p;
       $pendidikan->tanggal_awal_p = $request->tanggal_awal_p;
       $pendidikan->tanggal_akhir_p = $request->tanggal_akhir_p;
       $pendidikan->domisili_p = $request->domisili_p;
       $pendidikan->user_id_p = $request->user_id_p;
       
     if ($request->hasFile('foto_p')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('foto_p')->move('foto_pendidikan/', 
            $request->file('foto_p')
            ->getClientOriginalName());
            $pendidikan->foto_p = $request->file('foto_p')
            ->getClientOriginalName();
            $pendidikan->save();
        }
        return back()->with('success', 'Anda Berhasil Tambah data Pendidikan');
   }

    public function add_pengalaman_kegiatan(pengalaman_kegiatan_request $request){
      
    $pengalaman_kegiatan = new pengalaman_kegiatan();
    $pengalaman_kegiatan->nama_kegiatan_p_kegiatan = $request->nama_kegiatan_p_kegiatan;
    $pengalaman_kegiatan->nama_perusahaan_p_kegiatan = $request->nama_perusahaan_p_kegiatan;
    $pengalaman_kegiatan->tanggal_awal_p_kegiatan = $request->tanggal_awal_p_kegiatan;
    $pengalaman_kegiatan->tanggal_akhir_p_kegiatan = $request->tanggal_akhir_p_kegiatan;
    $pengalaman_kegiatan->domisili_p_kegiatan = $request->domisili_p_kegiatan;
    $pengalaman_kegiatan->user_id_p_kegiatan = $request->user_id_p_kegiatan;
    if ($request->hasFile('foto_p_kegiatan')) //Cek apakah ada file avatar yang di Upload
    {
        $request->file('foto_p_kegiatan')->move('foto_kegiatan/', 
        $request->file('foto_p_kegiatan')
        ->getClientOriginalName());
        $pengalaman_kegiatan->foto_p_kegiatan = $request->file('foto_p_kegiatan')
        ->getClientOriginalName();
        $pengalaman_kegiatan->save();
    }
    return back()->with('success', 'Anda Berhasil Tambah Data Pengalaman Kegiatan');
   
    }   

    public function add_pengalaman_kerja(pengalaman_kerja_request $request){
      
        $pengalaman_kerja = new pengalaman_kerja();
        $pengalaman_kerja->nama_pengalaman_kerja = $request->nama_pengalaman_kerja;
        $pengalaman_kerja->nama_perusahaan_p_kerja = $request->nama_perusahaan_p_kerja;
        $pengalaman_kerja->tanggal_mulai_p_kerja = $request->tanggal_mulai_p_kerja;
        $pengalaman_kerja->tanggal_akhir_p_kerja = $request->tanggal_akhir_p_kerja;
        $pengalaman_kerja->domisili_p_kerja = $request->domisili_p_kerja;
        $pengalaman_kerja->user_id_p_kerja = $request->user_id_p_kerja;
        if ($request->hasFile('foto_p_kerja')) //Cek apakah ada file avatar yang di Upload
        {
          
            $request->file('foto_p_kerja')->move('foto_pengalaman_kerja/', 
            $request->file('foto_p_kerja')
            ->getClientOriginalName());
            $pengalaman_kerja->foto_p_kerja = $request->file('foto_p_kerja')
            ->getClientOriginalName();
            $pengalaman_kerja->save();
           
        }  
        return back()->with('success', 'Anda Berhasil Tambah Data Pengalaman Kerja');
    }
    public function add_prestasi(prestasi_request $request){
      
        $prestasi = new prestasi();
        $prestasi->nama_prestasi = $request->nama_prestasi;
        $prestasi->nama_perusahaan_prestasi = $request->nama_perusahaan_prestasi;
        $prestasi->tanggal_awal_prestasi = $request->tanggal_awal_prestasi;
        $prestasi->tanggal_akhir_prestasi = $request->tanggal_akhir_prestasi;
        $prestasi->domisili_prestasi = $request->domisili_prestasi;
        $prestasi->user_id_prestasi = $request->user_id_prestasi;
        if ($request->hasFile('foto_prestasi')) //Cek apakah ada file avatar yang di Upload
        {
          
            $request->file('foto_prestasi')->move('foto_prestasi/', 
            $request->file('foto_prestasi')
            ->getClientOriginalName());
            $prestasi->foto_prestasi = $request->file('foto_prestasi')
            ->getClientOriginalName();
            $prestasi->save();
          
        }
        return back()->with('success','Data Berhasil Input Prestasi');
    }

    public function add_sertifikat(sertifikat_request $request){
       
        $sertifikat = new sertifikat();
        $sertifikat->nama_sertifikat = $request->nama_sertifikat;
        $sertifikat->nama_perusahaan_sertifikat = $request->nama_perusahaan_sertifikat;
        $sertifikat->tanggal_awal_sertifikat = $request->tanggal_awal_sertifikat;
        $sertifikat->tanggal_akhir_sertifikat = $request->tanggal_akhir_sertifikat;
        $sertifikat->domisili_sertifikat = $request->domisili_sertifikat;
        $sertifikat->user_id_sertifikat =  $request->user_id_sertifikat;
        $sertifikat->update();
        if ($request->hasFile('foto_sertifikat')) //Cek apakah ada file avatar yang di Upload
        {
          
            $request->file('foto_sertifikat')->move('foto_sertifikat/', 
            $request->file('foto_sertifikat')
            ->getClientOriginalName());
            $sertifikat->foto_sertifikat = $request->file('foto_sertifikat')
            ->getClientOriginalName();
            $sertifikat->save();
          
        }
        return back()->with('success', 'Anda Berhasil Tambah Data Sertifikat');
      
    }
    public function update_pendidikan(Request $request, $id){
        $pendidikan = pendidikan::find($id);
        $pendidikan->nama_pendidikan_p = $request->nama_pendidikan_p;
        $pendidikan->nama_universitas_p = $request->nama_universitas_p;
        $pendidikan->tanggal_awal_p = $request->tanggal_awal_p;
        $pendidikan->tanggal_akhir_p = $request->tanggal_akhir_p;
        $pendidikan->domisili_p = $request->domisili_p;
        $pendidikan->update();
     
        if ($request->hasFile('foto_p')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $pendidikan->foto_p;
            $file_path = public_path('foto_pendidikan/' . $file_name);
            File::delete($file_path);
            $request->file('foto_p')->move('foto_pendidikan/', 
            $request->file('foto_p')
            ->getClientOriginalName());
            $pendidikan->foto_p = $request->file('foto_p')
            ->getClientOriginalName();
            $pendidikan->save();
           
        }
        return back()->with('success', 'Anda Berhasil Update data Pendidikan');
    }

    public function update_pengalaman_kegiatan(Request $request, $id){
        $pengalaman_kegiatan = pengalaman_kegiatan::find($id);
        $pengalaman_kegiatan->nama_kegiatan_p_kegiatan = $request->nama_kegiatan_p_kegiatan;
        $pengalaman_kegiatan->nama_perusahaan_p_kegiatan = $request->nama_perusahaan_p_kegiatan;
        $pengalaman_kegiatan->tanggal_awal_p_kegiatan = $request->tanggal_awal_p_kegiatan;
        $pengalaman_kegiatan->tanggal_akhir_p_kegiatan = $request->tanggal_akhir_p_kegiatan;
        $pengalaman_kegiatan->domisili_p_kegiatan = $request->domisili_p_kegiatan;
        $pengalaman_kegiatan->update();
        if ($request->hasFile('foto_p_kegiatan')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $pengalaman_kegiatan->foto_p_kegiatan;
            $file_path = public_path('foto_kegiatan/' . $file_name);
            File::delete($file_path);
            $request->file('foto_p_kegiatan')->move('foto_kegiatan/', 
            $request->file('foto_p_kegiatan')
            ->getClientOriginalName());
            $pengalaman_kegiatan->foto_p_kegiatan = $request->file('foto_p_kegiatan')
            ->getClientOriginalName();
            $pengalaman_kegiatan->save();
           
        }
        return back()->with('success', 'Anda Berhasil Update data Pendidikan');
    }                 
    
    public function update_prestasi(Request $request, $id){
        $prestasi = prestasi::find($id);
        $prestasi->nama_prestasi = $request->nama_prestasi;
        $prestasi->nama_perusahaan_prestasi = $request->nama_perusahaan_prestasi;
        $prestasi->tanggal_awal_prestasi = $request->tanggal_awal_prestasi;
        $prestasi->tanggal_akhir_prestasi = $request->tanggal_akhir_prestasi;
        $prestasi->domisili_prestasi = $request->domisili_prestasi;
        $prestasi->update();
        if ($request->hasFile('foto_prestasi')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $prestasi->foto_prestasi;
            $file_path = public_path('foto_prestasi/' . $file_name);
            File::delete($file_path);
            $request->file('foto_prestasi')->move('foto_prestasi/', 
            $request->file('foto_prestasi')
            ->getClientOriginalName());
            $prestasi->foto_prestasi = $request->file('foto_prestasi')
            ->getClientOriginalName();
            $prestasi->save();
        
        }
        return back()->with('success', 'Anda Berhasil Update data Prestasi');
    }
    public function update_pengalaman_kerja(Request $request,$id){
        $pengalaman = pengalaman_kerja::find($id);
        $pengalaman->nama_pengalaman_kerja = $request->nama_pengalaman_kerja;
        $pengalaman->nama_perusahaan_p_kerja = $request->nama_perusahaan_p_kerja;
        $pengalaman->tanggal_mulai_p_kerja = $request->tanggal_mulai_p_kerja;
        $pengalaman->tanggal_akhir_p_kerja = $request->tanggal_akhir_p_kerja;
        $pengalaman->domisili_p_kerja = $request->domisili_p_kerja;
        $pengalaman->update();
        if ($request->hasFile('foto_p_kerja')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $pengalaman->foto_p_kerja;
            $file_path = public_path('foto_pengalaman_kerja/' . $file_name);
            File::delete($file_path);
            $request->file('foto_p_kerja')->move('foto_pengalaman_kerja/', 
            $request->file('foto_p_kerja')
            ->getClientOriginalName());
            $pengalaman->foto_p_kerja = $request->file('foto_p_kerja')
            ->getClientOriginalName();
            $pengalaman->save();
          
        }
        return back()->with('success', 'Anda Berhasil Update data Pengalaman kerja');
    }
    
    public function update_sertifikat(Request $request,$id){
        $sertifikat = sertifikat::find($id);
        $sertifikat->nama_sertifikat =  $request->nama_sertifikat;
        $sertifikat->nama_perusahaan_sertifikat = $request->nama_perusahaan_sertifikat;
        $sertifikat->tanggal_awal_sertifikat = $request->tanggal_awal_sertifikat;
        $sertifikat->tanggal_akhir_sertifikat = $request->tanggal_akhir_sertifikat;
        $sertifikat->domisili_sertifikat =$request->domisili_sertifikat;
        $sertifikat->update();                      
        if ($request->hasFile('foto_sertifikat')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $sertifikat->foto_sertifikat;
            $file_path = public_path('foto_sertifikat/'.$file_name);
            File::delete($file_path);
            $request->file('foto_sertifikat')->move('foto_sertifikat/', 
            $request->file('foto_sertifikat')
            ->getClientOriginalName());
            $sertifikat->foto_sertifikat = $request->file('foto_sertifikat')
            ->getClientOriginalName();
            $sertifikat->save();
           
        }
        return back()->with('success', 'Anda Berhasil Update data Sertifikat');
    }
    public function delete_pendidikan($id){
      
        $pendidikan = pendidikan::find($id);
        $pendidikan->delete();
        $file_name = $pendidikan->foto_p;
        $file_path = public_path('foto_pendidikan/'.$file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Pendidikan');
    }
    public function delete_pengalaman_kegiatan($id){
        $pengalaman_kegiatan = pengalaman_kegiatan::find($id);
        $pengalaman_kegiatan->delete();
        $file_name = $pengalaman_kegiatan->foto_p_kegiatan;
        $file_path = public_path('foto_kegiatan/'.$file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Pengalaman Kegiatan');
        
    }
    public function delete_pengalaman_kerja($id){
        $pengalaman_kerja = pengalaman_kerja::find($id);
        $pengalaman_kerja->delete();
        $file_name = $pengalaman_kerja->foto_p_kerja;
        $file_path = public_path('foto_pengalaman_kerja/'.$file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Pengalaman Kerja');
    }
    public function delete_prestasi($id){
        $prestasi = prestasi::find($id);
        $prestasi->delete();
        $file_name = $prestasi->foto_prestasi;
        $file_path = public_path('foto_prestasi/'.$file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Prestasi');
    }
    public function delete_sertifikat($id){
    $sertifikat = sertifikat::find($id);
    $sertifikat->delete();
    $file_name = $sertifikat->foto_sertifikat;
    $file_path = public_path('foto_sertifikat/'.$file_name);
    File::delete($file_path);
    return back()->with('success', 'Anda Berhasil Delete Data Sertifikat');
    }
   public function edit_tentang (Request $request, $id){
       $user = user_pengguna::find($id);
        $user->tentang = $request->tentang;
        $user->update();
        return back()->with('success','Anda Berhasil Update data Tentang');     

   }
   public function edit_profile(Request $request, $id){
   
    $user = user_pengguna::find($id);
    $user->username = $request->username;
    $user->no_hp = $request->no_hp;
    if ($request->hasFile('foto_user')) //Cek apakah ada file avatar yang di Upload
    {
        $file_name = $user->foto_user;
        $file_path = public_path('foto_user/'.$file_name);
        File::delete($file_path);
        $request->file('foto_user')->move('foto_user/', 
        $request->file('foto_user')
        ->getClientOriginalName());
        $user->foto_user = $request->file('foto_user')
        ->getClientOriginalName();
        $user->save();
    }
   
    return back()->with('success','Anda Berhasil Update data Profile');    
   }
   public function delete_profile($id){
        $users = user_pengguna::find($id);
        $users->delete();
        $file_name = $users->foto_user;
        $file_path = public_path('foto_user/'.$file_name);
        File::delete($file_path);
        return back()->with('success','Anda Berhasil Delete data User');     
    }
   public function superadmin(){
       $kunjungan_halaman_utama =  kunjungan_halaman_utama::where('id','=',1)->first(); 
       $kunjungan_halaman_admin_user =  kunjungan_halaman_admin_user::where('id','=',1)->first();
       $kunjungan_halaman_biodata_user = kunjungan_biodata_user::where('id','=',1)->first();
       $users = user_pengguna::count();                                                    
       return view('Superadmin.index',compact('users','kunjungan_halaman_utama','kunjungan_halaman_admin_user','kunjungan_halaman_biodata_user'));
   }
   public function superadmin_user(){
    $users = DB::table('user_penggunas')
    ->select('username','email','no_hp','foto_user','id')
    ->paginate(5);
    return view('superadmin.user',['users'=>$users]);
   }
   public function login_superadmin(){
       return view('login_superadmin.index');
   }
   public function proses_login_administrator(Request $request){
    request()->validate([
        'username'=>'required',
        'password'=>'required',
    ]);
    $user = superadmin::where('username','=',$request->username)->first();
    if($user){
        if(Hash::check($request->password,$user->password)){
            $request->session()->put('superadminID',$user->id);
            return redirect('superadmin'); 
        }
        else{
            return redirect()->back()->with('fail','password Anda salah');
        }
    }else{
        return redirect()->back()->with('fail','Username Tidak Terdaftar');
    }        
    } 
    public function data_superadmin(){
        $superadmin = DB::table('superadmins')
                     ->select('username','password')
                     ->get();
        return view('Superadmin.data_superadmin',["superadmin"=>$superadmin]);
    } 
    public function tambah_data_superadmin(Request $request){
         request()->validate([
            'username'=>'required',
            'password' => 'required'
         ]);
         $superadmin = new superadmin();
         $superadmin->create([
             'username' => $request->username,
             'password' => Hash::make($request->password)
         ])->save();
         return back();
    }  
}

