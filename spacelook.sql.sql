-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Apr 2022 pada 08.07
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spacelook`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan_biodata_users`
--

CREATE TABLE `kunjungan_biodata_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jumlah_kunjungan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kunjungan_biodata_users`
--

INSERT INTO `kunjungan_biodata_users` (`id`, `jumlah_kunjungan`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan_halaman_admin_users`
--

CREATE TABLE `kunjungan_halaman_admin_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jumlah_kunjungan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kunjungan_halaman_admin_users`
--

INSERT INTO `kunjungan_halaman_admin_users` (`id`, `jumlah_kunjungan`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '2022-04-04 23:01:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan_halaman_utamas`
--

CREATE TABLE `kunjungan_halaman_utamas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jumlah_kunjungan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kunjungan_halaman_utamas`
--

INSERT INTO `kunjungan_halaman_utamas` (`id`, `jumlah_kunjungan`, `created_at`, `updated_at`) VALUES
(1, 11, NULL, '2022-04-12 23:59:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_03_24_024619_create_pendidikans_table', 1),
(6, '2022_03_24_024634_create_pengalaman_kerjas_table', 1),
(7, '2022_03_24_024650_create_prestasis_table', 1),
(8, '2022_03_24_031914_create_sertifikats_table', 1),
(9, '2022_03_24_032212_create_pengalaman_kegiatans_table', 1),
(10, '2022_03_24_034003_create_user_penggunas_table', 2),
(11, '2022_04_05_055035_create_kunjungan_halaman_utamas_table', 3),
(12, '2022_04_05_055049_create_kunjungan_biodata_users_table', 3),
(13, '2022_04_05_055115_create_kunjungan_halaman_admin_users_table', 3),
(14, '2022_04_06_023720_create_superadmins_table', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikans`
--

CREATE TABLE `pendidikans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id_p` int(11) NOT NULL,
  `nama_pendidikan_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_universitas_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_awal_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_akhir_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domisili_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengalaman_kegiatans`
--

CREATE TABLE `pengalaman_kegiatans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id_p_kegiatan` int(11) NOT NULL,
  `nama_kegiatan_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_awal_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_akhir_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domisili_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_p_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengalaman_kegiatans`
--

INSERT INTO `pengalaman_kegiatans` (`id`, `user_id_p_kegiatan`, `nama_kegiatan_p_kegiatan`, `nama_perusahaan_p_kegiatan`, `tanggal_awal_p_kegiatan`, `tanggal_akhir_p_kegiatan`, `domisili_p_kegiatan`, `foto_p_kegiatan`, `created_at`, `updated_at`) VALUES
(4, 1, 'Bakti Sosial', 'PT. Rebuff Mediatama', '2015-01-25', '2020-01-25', 'Jakarta Raya', 'proteksi formula 5000.PNG', '2022-04-02 06:14:30', '2022-04-02 06:14:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengalaman_kerjas`
--

CREATE TABLE `pengalaman_kerjas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id_p_kerja` int(11) NOT NULL,
  `nama_pengalaman_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan_p_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_mulai_p_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_akhir_p_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domisili_p_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_p_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengalaman_kerjas`
--

INSERT INTO `pengalaman_kerjas` (`id`, `user_id_p_kerja`, `nama_pengalaman_kerja`, `nama_perusahaan_p_kerja`, `tanggal_mulai_p_kerja`, `tanggal_akhir_p_kerja`, `domisili_p_kerja`, `foto_p_kerja`, `created_at`, `updated_at`) VALUES
(8, 1, 'Digital Marketing', 'Tokopedia', '2020-01-25', '2022-01-25', 'pontianak, kalimantan barat', 'formula triple action.PNG', '2022-04-02 06:36:54', '2022-04-02 06:36:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasis`
--

CREATE TABLE `prestasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id_prestasi` int(11) NOT NULL,
  `nama_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_awal_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_akhir_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domisili_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_prestasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `prestasis`
--

INSERT INTO `prestasis` (`id`, `user_id_prestasi`, `nama_prestasi`, `nama_perusahaan_prestasi`, `tanggal_awal_prestasi`, `tanggal_akhir_prestasi`, `domisili_prestasi`, `foto_prestasi`, `created_at`, `updated_at`) VALUES
(8, 1, 'Bootcamp Laravel', 'Digital Skola', '2015-01-25', '2020-02-25', 'pontianak, kalimantan barat', 'Amanah.png', '2022-04-02 06:31:46', '2022-04-02 06:35:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sertifikats`
--

CREATE TABLE `sertifikats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id_sertifikat` int(11) NOT NULL,
  `nama_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_awal_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_akhir_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domisili_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sertifikats`
--

INSERT INTO `sertifikats` (`id`, `user_id_sertifikat`, `nama_sertifikat`, `nama_perusahaan_sertifikat`, `tanggal_awal_sertifikat`, `tanggal_akhir_sertifikat`, `domisili_sertifikat`, `foto_sertifikat`, `created_at`, `updated_at`) VALUES
(5, 1, 'Leadership', 'Tokopedia', '2020-01-25', '2022-10-20', 'pontianak, kalimantan barat', 'formula Junior.PNG', '2022-04-02 06:36:23', '2022-04-02 06:36:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `superadmins`
--

CREATE TABLE `superadmins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `superadmins`
--

INSERT INTO `superadmins` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(3, 'superadmins', '$2y$10$RYUO9H5mUrUsVE0RbwDnL.g4Q5V3uOKafdU1rK2xIbR4rRCepyz.a', '2022-04-05 21:40:17', '2022-04-05 21:40:17'),
(4, 'superadmin', '$2y$10$OtqrfmqdEC1okD8qmDpjv.jLOXBotMUJ0LrnHc/828ESkfmK3J/oq', '2022-04-10 23:31:40', '2022-04-10 23:31:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_penggunas`
--

CREATE TABLE `user_penggunas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tentang` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user_penggunas`
--

INSERT INTO `user_penggunas` (`id`, `username`, `email`, `no_hp`, `tentang`, `foto_user`, `password`, `created_at`, `updated_at`) VALUES
(1, 'willyhp', 'limwilly0@gmail.com', '087734635840', 'Spacelook', 'formula triple action.PNG', '$2y$10$6evQFPuSgS0z46znCFkbb.7hYHF4OTOKs2ELEfOCTLGYMvMg8UdHy', '2022-03-27 00:45:33', '2022-04-04 21:58:09'),
(4, 'willy', 'willy@gmail.com', '087734635840', NULL, 'Capture.PNG', '$2y$10$zT37Z0IOk666oZEYljJ8IO0JDB54rer5PM4tXB.QhZK9kHRfnNgWS', '2022-04-02 06:04:25', '2022-04-02 06:04:25');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `kunjungan_biodata_users`
--
ALTER TABLE `kunjungan_biodata_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan_halaman_admin_users`
--
ALTER TABLE `kunjungan_halaman_admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan_halaman_utamas`
--
ALTER TABLE `kunjungan_halaman_utamas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pendidikans`
--
ALTER TABLE `pendidikans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengalaman_kegiatans`
--
ALTER TABLE `pengalaman_kegiatans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengalaman_kerjas`
--
ALTER TABLE `pengalaman_kerjas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `prestasis`
--
ALTER TABLE `prestasis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sertifikats`
--
ALTER TABLE `sertifikats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `superadmins`
--
ALTER TABLE `superadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `user_penggunas`
--
ALTER TABLE `user_penggunas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kunjungan_biodata_users`
--
ALTER TABLE `kunjungan_biodata_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kunjungan_halaman_admin_users`
--
ALTER TABLE `kunjungan_halaman_admin_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kunjungan_halaman_utamas`
--
ALTER TABLE `kunjungan_halaman_utamas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `pendidikans`
--
ALTER TABLE `pendidikans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pengalaman_kegiatans`
--
ALTER TABLE `pengalaman_kegiatans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pengalaman_kerjas`
--
ALTER TABLE `pengalaman_kerjas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `prestasis`
--
ALTER TABLE `prestasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `sertifikats`
--
ALTER TABLE `sertifikats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `superadmins`
--
ALTER TABLE `superadmins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_penggunas`
--
ALTER TABLE `user_penggunas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
